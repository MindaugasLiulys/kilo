<?php declare(strict_types=1);

use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\PaymentServiceProviderController;

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'payments'], function() {
    Route::post('/subscriptionWebhook', [PaymentServiceProviderController::class, 'subscriptionWebhook']);
});
