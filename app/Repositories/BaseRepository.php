<?php declare(strict_types=1);

namespace App\Repositories;

use Exception;
use Illuminate\Database\Eloquent\Model;

class BaseRepository
{
    protected $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function find(int $id): ?Model
    {
        return $this->model::query()
            ->find($id);
    }

    public function create(array $attributes): Model
    {
        return $this->model::query()
            ->create($attributes);
    }

    public function update(int $id,array $attributes): bool
    {
        return $this->model::query()
            ->find($id)
            ->update($attributes);
    }

    public function destroy(int $id): bool
    {
        try {
            return $this->find($id)->delete();
        } catch (Exception $e) {
            return false;
        }
    }
}
