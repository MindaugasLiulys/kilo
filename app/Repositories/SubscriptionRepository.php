<?php declare(strict_types=1);

namespace App\Repositories;

use App\Models\Subscription;
use Illuminate\Database\Eloquent\Model;

class SubscriptionRepository extends BaseRepository
{
    protected $model;

    public function __construct(Subscription $subscription)
    {
        parent::__construct($subscription);

        $this->model = $subscription;
    }

    public function getSubscriptionByIdentifier(string $identifier): ?Model
    {
        return $this->model->query()
            ->where('identifier', $identifier)
            ->first();
    }
}
