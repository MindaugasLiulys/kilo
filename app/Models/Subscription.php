<?php declare(strict_types=1);

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int id
 * @property string identifier
 * @property int auto_renew
 * @property int expiration_intent
 * @property int action
 * @property string transaction_id
 * @property Carbon ends_at
 */
class Subscription extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'subscriptions';

    protected $fillable = [
        'identifier',
        'auto_renew',
        'expiration_intent',
        'action',
        'transaction_id',
        'ends_at',
    ];

    protected $dates = [
        'ends_at',
    ];
}
