<?php declare(strict_types=1);

namespace App\Http\Services;

use App\Models\Subscription;
use App\Repositories\SubscriptionRepository;

class SubscriptionService
{
    protected $subscriptionRepository;

    public function __construct(
        SubscriptionRepository $subscriptionRepository
    )
    {
        $this->subscriptionRepository = $subscriptionRepository;
    }

    public function callInitialBuyEvent(array $normalizedData): void
    {
        $this->subscriptionRepository->create($normalizedData);

        try {
//          TODO: Call method that gives user the ability to access product
//          TODO: Do the remaining steps related to the product
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function callDidRenewEvent(array $normalizedData): void
    {
        /** @var Subscription $subscription */
        $subscription = $this->subscriptionRepository->getSubscriptionByIdentifier($normalizedData['identifier']);
        $this->subscriptionRepository->update($subscription->id, $normalizedData);

        try {
//          TODO: Do the remaining steps related to the product
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function callDidFailedToRenewEvent(array $normalizedData): void
    {
        /** @var Subscription $subscription */
        $subscription = $this->subscriptionRepository->getSubscriptionByIdentifier($normalizedData['identifier']);
        $this->subscriptionRepository->update($subscription->id, $normalizedData);

        try {
//          TODO: Call method that removed the ability to access product from the user
//          TODO: Do the remaining steps related to the product
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function callCancelEvent(array $normalizedData): void
    {
        /** @var Subscription $subscription */
        $subscription = $this->subscriptionRepository->getSubscriptionByIdentifier($normalizedData['identifier']);
        $this->subscriptionRepository->update($subscription->id, $normalizedData);
//        $this->subscriptionRepository->destroy($subscription->id); // Not sure if this is necessary

        try {
//          TODO: Call method that removed the ability to access product from the user
//          TODO: Do the remaining steps related to the product
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }
}
