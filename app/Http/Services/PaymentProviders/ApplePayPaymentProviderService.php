<?php declare(strict_types=1);

namespace App\Http\Services\PaymentProviders;

use App\Enum\NotificationTypeEnum;
use App\Enum\PaymentExpirationEnum;
use Carbon\Carbon;

class ApplePayPaymentProviderService
{
    public function normalize(array $data): array
    {
        return [
            'identifier' => $data['auto_renew_product_id'],
            'auto_renew' => (int)filter_var($data['auto_renew_status'], FILTER_VALIDATE_BOOLEAN),
            'expiration_intent' => $this->mapPaymentExpirationEnum((int)$data['expiration_intent']),
            'action' => $this->mapNotificationTypeEnum($data['notification_type']),
            'end_at' => Carbon::parse($data['unified_receipt']['pending_renewal_info']['grace_period_expires_date']),
            'transaction_id' => $data['unified_receipt']['pending_renewal_info']['original_transaction_id'],
        ];
    }

    private function mapPaymentExpirationEnum(int $type): int
    {
        switch ($type) {
            case 1:
                return PaymentExpirationEnum::CANCELED;
            case 2:
                return PaymentExpirationEnum::BILLING_ERROR;
            case 3:
                return PaymentExpirationEnum::REFUSED_PRICE;
            case 4:
                return PaymentExpirationEnum::PRODUCT_NOT_AVAILABLE;
            case 5:
                return PaymentExpirationEnum::ERROR;
            default:
                return PaymentExpirationEnum::UNKNOWN;
        }
    }

    private function mapNotificationTypeEnum(string $type): int
    {
        switch ($type) {
            case 'CANCEL':
                return NotificationTypeEnum::CANCEL;
            case 'DID_CHANGE_RENEWAL_PREF':
                return NotificationTypeEnum::DID_CHANGE_RENEWAL_PREF;
            case 'DID_CHANGE_RENEWAL_STATUS':
                return NotificationTypeEnum::DID_CHANGE_RENEWAL_STATUS;
            case 'DID_FAIL_TO_RENEW':
                return NotificationTypeEnum::DID_FAIL_TO_RENEW;
            case 'DID_RECOVER':
                return NotificationTypeEnum::DID_RECOVER;
            case 'DID_RENEW':
                return NotificationTypeEnum::DID_RENEW;
            case 'INITIAL_BUY':
                return NotificationTypeEnum::INITIAL_BUY;
            case 'INTERACTIVE_RENEWAL':
                return NotificationTypeEnum::INTERACTIVE_RENEWAL;
            case 'PRICE_INCREASE_CONSENT':
                return NotificationTypeEnum::PRICE_INCREASE_CONSENT;
            case 'REFUND':
                return NotificationTypeEnum::REFUND;
            case 'RENEWAL':
                return NotificationTypeEnum::RENEWAL;
            case 'REVOKE':
                return NotificationTypeEnum::REVOKE;
            default:
                return NotificationTypeEnum::UNKNOWN;
        }
    }
}
