<?php declare(strict_types=1);

namespace App\Http\Services\PaymentProviders;

use App\Enum\PaymentServiceProviderEnum;

class PaymentProviderService
{
    protected $applePayPaymentProviderService;

    public function __construct(
        ApplePayPaymentProviderService $applePayPaymentProviderService
    ) {
        $this->applePayPaymentProviderService = $applePayPaymentProviderService;
    }

    public function checkPaymentServiceProviderType(array $data): int
    {
        $type = 0;
        if (isset($data['auto_renew_adam_id']) && $data['auto_renew_adam_id']) {
            $type = PaymentServiceProviderEnum::APPLE_PAY;
        }

//        TODO: Add more checks for different payment service providers

        return $type;
    }

    public function normalizeRequestData(int $paymentProviderType, array $data): array
    {
        switch($paymentProviderType) {
            case PaymentServiceProviderEnum::APPLE_PAY:
                return $this->applePayPaymentProviderService->normalize($data);
//          TODO: Add additional cases for different payment service providers
            default:
                return [];
            }

    }
}
