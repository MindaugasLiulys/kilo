<?php declare(strict_types=1);

namespace App\Http\Controllers;

use App\Enum\NotificationTypeEnum;
use App\Http\Services\PaymentProviders\PaymentProviderService;
use App\Http\Services\SubscriptionService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PaymentServiceProviderController extends Controller
{
    protected $paymentProviderService;
    protected $subscriptionService;

    public function __construct(
        PaymentProviderService $paymentProviderService,
        SubscriptionService $subscriptionService
    )
    {
        $this->paymentProviderService = $paymentProviderService;
        $this->subscriptionService = $subscriptionService;
    }

    public function subscriptionWebhook(Request $request): Response
    {
        $paymentProviderType = $this->paymentProviderService->checkPaymentServiceProviderType($request->all());

        if ($paymentProviderType) {
            $normalizedData = $this->paymentProviderService->normalizeRequestData($paymentProviderType, $request->all());

            try {
                switch ($normalizedData['action']) {
                    case NotificationTypeEnum::INITIAL_BUY:
                        $this->subscriptionService->callInitialBuyEvent($normalizedData);
                        break;
                    case NotificationTypeEnum::DID_RENEW:
                        $this->subscriptionService->callDidRenewEvent($normalizedData);
                        break;
                    case NotificationTypeEnum::DID_FAIL_TO_RENEW:
                        $this->subscriptionService->callDidFailedToRenewEvent($normalizedData);
                        break;
                    case NotificationTypeEnum::CANCEL:
                        $this->subscriptionService->callCancelEvent($normalizedData);
                        break;
                }

                return response('OK', 200);
            } catch (\Exception $e) {
                throw new \Exception($e->getMessage());
            }
        }

        return response('FAILED', 500);
    }
}
