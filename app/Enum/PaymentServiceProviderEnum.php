<?php declare(strict_types=1);

namespace App\Enum;

class PaymentServiceProviderEnum
{
    public const APPLE_PAY = 1;
    public const STRIPE = 2;
    public const PAYPAL = 3;
}
