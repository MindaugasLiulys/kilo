<?php declare(strict_types=1);

namespace App\Enum;

class PaymentExpirationEnum
{
    public const UNKNOWN = 0;
    public const CANCELED = 1;
    public const BILLING_ERROR = 2;
    public const REFUSED_PRICE = 3;
    public const PRODUCT_NOT_AVAILABLE = 4;
    public const ERROR = 5;
}
