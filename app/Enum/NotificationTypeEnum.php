<?php declare(strict_types=1);

namespace App\Enum;

class NotificationTypeEnum
{
    public const UNKNOWN = 0;
    public const CANCEL = 1;
    public const DID_CHANGE_RENEWAL_PREF = 2;
    public const DID_CHANGE_RENEWAL_STATUS = 3;
    public const DID_FAIL_TO_RENEW = 4;
    public const DID_RECOVER = 5;
    public const DID_RENEW = 6;
    public const INITIAL_BUY = 7;
    public const INTERACTIVE_RENEWAL = 8;
    public const PRICE_INCREASE_CONSENT = 9;
    public const REFUND = 10;
    public const RENEWAL = 11;
    public const REVOKE = 12;
}
